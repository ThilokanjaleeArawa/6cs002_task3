public class StrategyPattern {

	public static void main(String[] args) {
	      Context context = new Context(new FirstActivity());		
	      context.executeStrategy();

	      context = new Context(new SecondActivity());		
	      context.executeStrategy();

	      context = new Context(new ThirdActivity());		
	      context.executeStrategy();
	   }
	
}
