import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class InsertPlayerName extends DisplayFrame{
	
	   public InsertPlayerName(Subject subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }

	   @Override
	   public void displayFrame() {
			label = new JLabel(MultiLinugualStringTable.getMessage(0));
			welcomeFrame.add(label);
			text = new JTextField(16); 
		    welcomeFrame.add(text);
		    button = new JButton("OK");
			welcomeFrame.add(button);
			button.addActionListener(this); 
			welcomeFrame.pack();
	   }

}
