public class AardvarkPattern {
	public static void main(String[] args) {
		MenuPlay model = new MenuPlay();
		MenuView view = new MenuView();
		MenuController controller = new MenuController(model, view);
		controller.updateView();
	}
}
