public class CommandInvoker {
	
	private InterfaceCommand placeDomino;
	
	public CommandInvoker(CommandDomino placeDominoCommand) {
		this.placeDomino = (InterfaceCommand) placeDominoCommand;
	}
	
	public void doPlaceDomino() {
		placeDomino.execute();
	}
	
	public void undoPlaceDomino() {
		placeDomino.unexecute();
	}

}
