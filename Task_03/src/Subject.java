import java.util.ArrayList;
import java.util.List;

public class Subject{
	
	private List<DisplayFrame> Moniter = new ArrayList<DisplayFrame>();

	   public void attach(DisplayFrame observer){
		   Moniter.add(observer);		
	   }

	   public void notifyAllObservers(){
	      for (DisplayFrame observer : Moniter) {
	         observer.displayFrame();
	      }
	   } 	
	
}

