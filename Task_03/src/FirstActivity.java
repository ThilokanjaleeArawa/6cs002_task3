public class FirstActivity implements InterfaceStrategy{
	
	   @Override
	   public void autoPlay() {
		   Aardvark aardvark = new Aardvark(1);
		   aardvark.playerName = "Level 1 Player";
		   aardvark.playGame();
	   }
	   
	}
