public class MenuView {
	public void displayGUIView() {
		  Subject subject = new Subject();

	      new MenuWelcome(subject);
	      new InsertPlayerName(subject);
	      new MainMenu(subject);
	      new MenuDifficulty(subject);
	      
	      subject.notifyAllObservers();
	}


}
