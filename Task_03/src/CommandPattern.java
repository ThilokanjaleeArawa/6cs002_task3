public class CommandPattern {

	public static void main(String[] args) {
		Aardvark ardvark = new Aardvark(1);
		
		CommandDomino placeDomino = new CommandDomino(ardvark);
		
		CommandInvoker invoker = new CommandInvoker(placeDomino);
		invoker.doPlaceDomino();
		
		System.out.println("\nUndo the domino placement (Y/N)?");
		String undo = IOLibrary.getString();
		if(undo.equalsIgnoreCase("Y")) {
			invoker.undoPlaceDomino();
		}
	}

}

