public class MenuController {
	private MenuPlay model;
	private MenuView view;
	
	public MenuController(MenuPlay model, MenuView view) {
		this.model = model;
		this.view = view;
	}
	public void setPlayerName(String name) {
		model.setName(name);
	}
	public String getPlayerName() {
		return model.getName();
	}
	public void updateView() {
		view.displayGUIView();
	}
}
